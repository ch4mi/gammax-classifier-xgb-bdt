import uproot3 as uproot
import pandas as pd

def dfFromKeys(filepath, RQList, treeName):
    """General purpose function to return a df given a file and RQs"""
    """Edit function to take in list of files"""
    
    dictFromKeys = dict.fromkeys(RQList)
    f = uproot.open(filepath)
    
    for RQ in RQList:
        dictFromKeys[RQ] = f[treeName + '/' + RQ].array()
        
    return pd.DataFrame().from_dict(dictFromKeys)

def radialCut(df, radiusCutValue):
    """Return a df with a radial cut"""
    
    squaredRad_ = df['x_cm']*df['x_cm'] + df['y_cm']*df['y_cm']
    return df[squaredRad_ < (radiusCutValue**2)]

def prepareDataset(dfSS, dfMSSI, trainingVars, commonRQs):
    # Remove -999.0 bad events.
    dfSS_copy = dfSS.copy(deep = True)
    dfMSSI_copy = dfMSSI.copy(deep = True)
    for RQ in trainingVars:
        dfSS_copy = dfSS_copy[dfSS_copy[RQ] != -999.0]
        dfMSSI_copy = dfMSSI_copy[dfMSSI_copy[RQ] != -999.0]

    # Combine the datasets
    numEvents = min(len(dfSS_copy), len(dfMSSI_copy))
    print('Number of events from each class: ' + str(numEvents))
    df = dfMSSI_copy[commonRQs].head(numEvents).append(dfSS_copy[commonRQs].head(numEvents))
    df = df.sample(frac = 1).reset_index(drop = True)

    # Plot the training RQs
    # pairplot = sns.pairplot(df.head(500), 
                            # vars = trainingVars,
                            # hue = 'MSSI')

    # Separate truth 
    y_df = df.pop('MSSI')
    
    return df, y_df

