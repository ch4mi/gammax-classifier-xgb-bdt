import skopt
from skopt.plots import plot_objective, plot_evaluations
from trainAndEval import trainAndEvaluate
import matplotlib.pyplot as plt

SPACE = [
    skopt.space.Integer(2, 10, name='max_depth', prior='uniform'),
    skopt.space.Real(0.01, 0.5, name='learning_rate', prior='log-uniform'),
    skopt.space.Real(0.1, 1.0, name='subsample', prior='uniform'),
    skopt.space.Real(0.1, 3.0, name='min_split_loss', prior='uniform'),
    skopt.space.Real(0.1, 2.0, name='lambda', prior='uniform'),
    skopt.space.Real(0.1, 1.0, name='colsample_bytree', prior='uniform'),
	]

@skopt.utils.use_named_args(SPACE)
def objective(**params):
	return -1.0 * trainAndEvaluate(params)

results = skopt.forest_minimize(objective, SPACE, n_calls = 2000, n_initial_points = 200, random_state = 42)
bestAUC = -1.0 * results.fun
bestParams = results.x

print('Best result: ', bestAUC)
print('Best parameters: ', bestParams)

_ = plot_evaluations(results)
plt.savefig('hpo_evaluations.pdf')

_ = plot_objective(results)
plt.savefig('hpo_objective.pdf')
