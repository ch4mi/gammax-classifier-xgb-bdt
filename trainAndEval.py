import pandas as pd
import xgboost
from xgboost import cv

import helper
randomState = 42

SEARCH_PARAMS = {'max_depth': 4, # Increasing this value will make the model more complex and likely to overfit.
                 'learning_rate': 0.3, # Shrinks the feature weights after boosting, has a range [0, 1]. Larger value makes the model conservative. 
                 'subsample': 1, # Subsample fraction before growing trees, making the model more conservative
                 'min_split_loss': 0.05, # This is "Gamma" in the XGBoost paper, a proposed split needs to improve the loss function by this amount 
                 'lambda': 1, # L2 regularization. Can think of this as preventing the BDT from separating leaf too aggressively. 
				 'colsample_bytree': 1, # How many features to use in each tree?
                }

def trainAndEvaluate(search_params):

	filepath = '/home/amarascs/LZ/HE-NR/GammaX Classifiers/BDT/DFs/'

	df = pd.read_pickle(filepath + "trainingDF.pkl")
	y_df = pd.read_pickle(filepath + "labelDF.pkl")

	print('Data loaded')

	trainingRQs = ['cluster_size_cm',
				   'log_MPAF',
				   'TopBottomAsymmetry']

	params = {'objective' :  'binary:logistic',
			  'eval_metric': 'logloss',
			  **search_params}

	# Use all data for cross validation
	dTrain = xgboost.DMatrix(df[trainingRQs], label = y_df)

	# Train the BDT with cross validation
	xgbCV = cv(params, dTrain, nfold = 4, num_boost_round = 1000, early_stopping_rounds = 10, metrics = 'auc', seed = randomState)

	return xgbCV['test-auc-mean'].max()
	
if __name__ == '__main__':
	score = trainAndEvaluate(SEARCH_PARAMS)
	print('validation AUC: ', score)
