Code for training an XGBoost BDT and running hyperparameter optimization using scikit-optimize on GreatLakes.

XGBoost documentation: https://xgboost.readthedocs.io/en/latest/index.html
Scikit-optimize (HPO wrapper) documentation: https://scikit-optimize.github.io/stable/

Run this code with `sbatch submit_HPO.sh` on GL. Requires a labeled dataframe with GX and SS scatters - see functions in `helper.py` for creation. 
